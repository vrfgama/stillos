<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    protected $fillable= [
        'id_cliente',
        'id_evento',
        'mesa_reservada'
    ];

    protected $table= 'reservas';
    /*
    public function cliente(){
        return $this->belongsTo(Cliente::class, 'id_cliente');
    }

    public function evento(){
        return $this->belongsTo(Evento::class, 'id_evento');
    }*/
}
