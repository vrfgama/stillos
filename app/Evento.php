<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $fillable= [
        'nome',
        'local',
        'rua',
        'numero',
        'bairro',
        'cidade',
        'cep',
        'data',
        'qtd_mesas'
    ];

    protected $table= 'eventos';
    /*
    public function reserva(){
        return $this->hasMany(Reserva::class, 'id_evento');
    }*/
}
