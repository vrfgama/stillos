<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evento;

class EventoController extends Controller
{    
    public $e;

    public function __construct(){
        $this->e= new Evento();
    }

    
    public function listaEvento(){
        return Evento::all();
    }

    public function getEvento($id){
        return $this->e->find($id);
    }
    
    public function index(){
        $eventos= Evento::all();
        return view('index', ['lista'=>$eventos]);
    }

    public function formulario(){
        return view('form_evento');
    }

    public function cadastrar(Request $r){
        Evento::create($r->all());
        return redirect('/stillos');
    }

    
    public function excluirView($id){
        return view('excluir_evento', ['evento' => $this->getEvento($id)] );
    }

    public function excluir($id){
        $this->getEvento($id)->delete();
        return redirect('/stillos');
    }

    public function atualizarView($id){
        return view('atualizar_evento', ['evento'=>$this->getEvento($id)]);
    }

    public function atualizar(Request $r){
        $e= $this->getEvento($r->id);
        $e->update($r->all());
        return redirect('/stillos');
    }

}
