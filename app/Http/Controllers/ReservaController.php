<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Evento;
use App\Reserva;
use DB;

class ReservaController extends Controller
{
    
    public function listaClientesView($id){
        $e= Evento::find($id);
        $lista_cl= Cliente::all();
        return view('lista_cliente_evento', ['e'=>$e, 'c'=>$lista_cl]);  
    }

    public function mesas($ev_id, $cl_id){
        $cl= Cliente::find($cl_id);
        $ev= Evento::find($ev_id);
        $r= Reserva::where('id_cliente','=' ,$cl->id)->where('id_evento', '=',$ev->id)->orderBy('mesa_reservada', 'ASC') ->get();

        return view('cliente_evento_reserva', ['c'=>$cl, 'e'=>$ev, 'm'=>$r]);
    }

    public function reservar($ev_id, $cl_id, Request $request){
        $ev= Evento::find($ev_id);
        $r= Reserva::where('id_cliente', '=', $cl_id )->where('id_evento', '=',$ev_id)->orderBy('mesa_reservada', 'ASC') ->get();
        
        $ctrl= true;
        foreach($r as $mesas){
            if($mesas->mesa_reservada == $request->res) $ctrl= false;   
        }

        if($request->res <= $ev->qtd_mesas && $request->res > 0 && $ctrl){
            Reserva::create(['id_cliente' => $cl_id, 'id_evento' => $ev_id, 'mesa_reservada'=>$request->res]);
            return $this->mesas($ev_id, $cl_id);
        }else{
            return view('erro');
        }
    }

    public function relatorio($id){
        
        $consulta= DB::table('eventos') ->join('reservas', 'eventos.id', '=', 'reservas.id_evento')
                                             ->join('clientes', 'reservas.id_cliente' ,'=', 'clientes.id') 
                                             ->select('eventos.id', 'reservas.mesa_reservada', 'clientes.id', 'clientes.nome')
                                             ->where('eventos.id', '=', $id)
                                             ->orderBy('reservas.mesa_reservada', 'ASC')
                                             ->get(); 
        
        $colecao= collect($consulta);
        return view('relatorio', ['rel'=>$colecao]);
    }

    public function removerView($ev_id, $cl_id) {
        $cl= Cliente::find($cl_id);
        $ev= Evento::find($ev_id);

        $r= Reserva::where('id_cliente', '=', $cl->id )->where('id_evento', '=', $ev->id )
                            ->orderBy('mesa_reservada', 'ASC') ->get();

        return view('remover_mesa', ['c'=>$cl, 'e'=>$ev, 'm'=>$r ] );
    }

    public function remover($ev_id, $cl_id, Request $request){
        $ev= Evento::find($ev_id);
        $r= Reserva::where('id_cliente', '=', $cl_id )->where('id_evento', '=',$ev_id)->orderBy('mesa_reservada', 'ASC') ->get();
        
        $ctrl= false;
        foreach($r as $mesas){
            if($mesas->mesa_reservada == $request->res) $ctrl= true;
        }

        if($request->res > 0 && $request->res <= $ev->qtd_mesas && $ctrl){

            $sql= DB::table('reservas')->select('id')->where('mesa_reservada', '=', $request->res)->get();
            $resp= collect($sql);
            
            DB::table('reservas')->where('id', '=', $resp[0]->id) ->delete();
            
            return $this->removerView($ev_id, $cl_id);
        }else{
            return view('erro');
        }
    }
}
