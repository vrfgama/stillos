<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;


class ClienteController extends Controller
{
    public $c;

    public function __construct(){
        $this->c= new Cliente();
    }
    
    public function listaCliente(){
        return Cliente::all();
    }

    public function getCliente($id){
        return $this->c->find($id);
    }

    public function lista(){
        $clientes= $this->listaCliente();
        return view('lista_clientes', ['lista'=>$clientes ]);
    }

    public function formulario(){
        return view('form_cliente');
    }

    public function cadastrar(Request $r){
        Cliente::create($r->all());
        return redirect('/stillos/lista_clientes'); //rota
    }
    
    public function excluirView($id){
        return view('excluir_cliente', ['cliente'=>$this->getCliente($id)]);
    }

    public function excluir($id){
        $this->getCliente($id)->delete();
        return redirect('/stillos/lista_clientes'); //rota
    }

    public function atualizarView($id){
        return view('atualizar_cliente', ['cliente'=>$this->getCliente($id)]);
    }

    public function atualizar(Request $r){
        $c= $this->getCliente($r->id);
        $c->update($r->all());
        return redirect('/stillos/lista_clientes');
    }

}
