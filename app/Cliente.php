<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable= [
        'nome',
        'cpf',
        'rua',
        'numero',
        'bairro',
        'cidade',
        'cep'
    ];

    protected $table= 'clientes';
    /*
    public function reserva(){
        return $this->hasMany(Reserva::class, 'id_cliente');
    }*/
}
