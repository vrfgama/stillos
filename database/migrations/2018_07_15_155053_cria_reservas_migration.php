<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaReservasMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cliente') -> unsigned();
            $table->integer('id_evento') -> unsigned();
            $table->integer('mesa_reservada');

            $table->foreign('id_cliente') -> references('id') -> on('clientes') -> onDelete('cascade');
            $table->foreign('id_evento') -> references('id') -> on('eventos') -> onDelete('cascade');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
