@extends('template.modelo')

@section('container')
    <h3>Seleção de clientes para reserva de mesa</h3>
    Evento: {{ $e->nome }} <br>
    Local: {{ $e->local }} <br>
    Qtd de mesas: {{ $e->qtd_mesas }} <br>
    <h3>Listagem de clientes</h3>
    @foreach($c as $cliente)
        <br>
        Nome: {{ $cliente->nome }} <br>
        CPF: {{ $cliente->cpf }} <br>
        Endereço <br>
        Rua: {{ $cliente->rua }} <br>
        Número: {{ $cliente->numero }} <br>
        Bairro: {{ $cliente->bairro }} <br>
        Cidade: {{ $cliente->cidade }} <br>
        CEP: {{ $cliente->cpf }} <br>
        <a href='{{ url("/stillos/reserva_mesa_evento/{$e->id}/{$cliente->id}") }}' >Reservar mesa para este cliente</a><br>
        <a href='{{ url("/stillos/remover_mesa_evento/{$e->id}/{$cliente->id}") }}' >Remover mesa reservada</a><br>
    @endforeach
@endsection