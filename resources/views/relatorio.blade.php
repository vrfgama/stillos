@extends('template.modelo')

@section('container')
    <h3>Mesas reservadas por cliente</h3><br>
    <br>

    @for($i=0; $i < count($rel); $i++)
        Mesa reservada: {{ $rel[$i]->mesa_reservada }}, 
        Cliente: {{$rel[$i]->nome}} <br>
    @endfor

    <br><input type='button' value='Voltar' onclick='history.go(-1)' />
@endsection