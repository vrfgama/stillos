@extends('template.modelo')

@section('container')
    <h3>Listagem de eventos</h3>
    @foreach($lista as $evento)
        <br>
        Nome: {{ $evento->nome }} <br>
        Local: {{ $evento->local }} <br>
        Endereço <br>
        Rua: {{ $evento->rua }} <br>
        Número: {{ $evento->numero }} <br>
        Bairro: {{ $evento->bairro }} <br>
        Cidade: {{ $evento->cidade }} <br>
        Data: {{ $evento->data }} <br>
        Qtd. de mesas: {{ $evento->qtd_mesas }} <br>
        <a href=' {{ url("/stillos/deletar/$evento->id") }} ' >Deletar</a><br>
        <a href=' {{ url("/stillos/atualizar/$evento->id") }} ' >Atualizar</a><br>
        <a href=' {{ url("/stillos/reserva_mesa/$evento->id") }}' >Reservas</a><br>
        <a href=' {{ url("/stillos/relatorio/$evento->id") }}' >Relatório de mesas reservadas</a><br>
    @endforeach
@endsection