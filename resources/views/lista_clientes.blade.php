@extends('template.modelo')

@section('container')
<h3>Listagem de clientes</h3>
    @foreach($lista as $cliente)
        <br>
        Nome: {{ $cliente->nome }} <br>
        CPF: {{ $cliente->cpf }} <br>
        Endereço <br>
        Rua: {{ $cliente->rua }} <br>
        Número: {{ $cliente->numero }} <br>
        Bairro: {{ $cliente->bairro }} <br>
        Cidade: {{ $cliente->cidade }} <br>
        CEP: {{ $cliente->cep }} <br>
        <a href=' {{ url("/stillos/del_cliente/$cliente->id") }} ' >Deletar</a><br>
        <a href=' {{ url("/stillos/atualizar_cliente/$cliente->id") }}' >Atualizar</a><br>
        
    @endforeach

@endsection