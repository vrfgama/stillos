@extends('template.modelo')

@section('container')
<h3>Atualização de dados cadastrais de eventos</h3>
    <form action=" {{ url('/stillos/editar_evento')}} " method="post" >
        {{ csrf_field() }}

        <input type='hidden' name="id" value="{{ $evento->id }}" >

        Nome: <br>
        <input type="text" name="nome" value="{{$evento->nome}}"><br>
        Local: <br>
        <input type="text" name="local" value="{{$evento->local}}"><br>
        Endereço: <br>
        Rua: <br>
        <input type="text" name="rua" value="{{$evento->rua}}"><br>
        Número: <br>
        <input type="text" name="numero" size="2" value="{{$evento->numero}}"><br>
        Bairro: <br>
        <input type="text" name="bairro" value="{{$evento->bairro}}"><br>
        Cidade: <br>
        <input type="text" name="cidade" value="{{$evento->cidade}}"><br>
        <!--
        CEP: <br>
        <input type="text" name="cep" size="8"><br>
        -->
        Data: <br>
        <input type="date" name="data" size="10" value="{{$evento->date}}"><br>
        Quantidade de mesas: <br>
        <input type="text" name="qtd_mesas" size="5" value="{{$evento->qtd_mesas}}"><br>
        <input type="submit" name="enviar" value="Enviar" >
        <input type='button' value='Cancelar' onclick='history.go(-1)' />
    </form>

@endsection