<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Stillo's</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <a href=" {{ url('/stillos/form_cliente') }}" />Cadastrar cliente</a>
    <a href=" {{ url('/stillos/lista_clientes') }}" />Consultar cliente</a>
    <a href=" {{ url('/stillos/form_evento') }}" />Cadastrar evento</a>
    <a href=" {{ url('/stillos/') }}" />Consultar eventos</a>
    @yield('container')
</body>
</html>