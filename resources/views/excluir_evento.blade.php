@extends('template.modelo')

@section('container')
    <h2>Deseja excluir este evento?</h2><br>
    Nome: {{ $evento->nome }} <br>
    Local: {{ $evento->local }} <br>
    Endereço <br>
    Rua: {{ $evento->rua }} <br>
    Número: {{ $evento->numero }} <br>
    Bairro: {{ $evento->bairro }} <br>
    Cidade: {{ $evento->cidade }} <br>
    Data: {{ $evento->data }} <br>
    Qtd. de mesas: {{ $evento->qtd_mesas }} <br>
    <a href=' {{ url("/stillos/excluir/$evento->id") }} ' class="href">Excluir</a><br>
    <a href=" {{ url('/stillos') }} " >Cancelar</a>
@endsection