@extends('template.modelo')

@section('container')

    <h2>Deseja excluir este cliente?</h2><br>
    Nome: {{ $cliente->nome }} <br>
    CPF: {{ $cliente->cpf }} <br>
    Endereço <br>
    Rua: {{ $cliente->rua }} <br>
    Número: {{ $cliente->numero }} <br>
    Bairro: {{ $cliente->bairro }} <br>
    Cidade: {{ $cliente->cidade }} <br>
    CEP: {{ $cliente->cep }} <br>
    <a href=' {{ url("/stillos/excluir_cliente/$cliente->id") }} '>Excluir</a><br>
    <a href=" {{ url('/stillos/lista_clientes') }} " >Cancelar</a>

@endsection