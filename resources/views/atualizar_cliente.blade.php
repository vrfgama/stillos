@extends('template.modelo')

@section('container')
<h3>Alteração de dados de cadastro de clientes</h3>
    <form action=" {{ url('/stillos/editar_cliente')}} " method="post" >
        {{ csrf_field() }}
        
        <input type='hidden' name="id" value="{{ $cliente->id }}" >
        
        Nome: <br>
        <input type="text" name="nome" value="{{ $cliente->nome }}"><br>
        CPF: <br>
        <input type="text" name="cpf" value="{{ $cliente->cpf }}" ><br>
        Endereço: <br>
        Rua: <br>
        <input type="text" name="rua" value="{{ $cliente->rua }}"><br>
        Número: <br>
        <input type="text" name="numero" size="2" value="{{ $cliente->numero }}"><br>
        Bairro: <br>
        <input type="text" name="bairro" value="{{ $cliente->bairro }}"><br>
        Cidade: <br>
        <input type="text" name="cidade" value="{{ $cliente->cidade }}"><br>
        CEP: <br>
        <input type="text" name="cep" size="8" value="{{ $cliente->cep }}"><br>
        <input type="submit" name="enviar" value="Alterar" >
        <input type='button' value='Cancelar' onclick='history.go(-1)' />
        
    </form>

@endSection