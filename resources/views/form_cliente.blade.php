@extends('template.modelo')

@section('container')
    <h3>Fomulário de cadastro de clientes</h3>
    <form action=" {{ url('/stillos/cad_cliente')}} " method="post" >
        {{ csrf_field() }}
        Nome: <br>
        <input type="text" name="nome" ><br>
        CPF: <br>
        <input type="text" name="cpf" ><br>
        Endereço: <br>
        Rua: <br>
        <input type="text" name="rua" ><br>
        Número: <br>
        <input type="text" name="numero" size="2"><br>
        Bairro: <br>
        <input type="text" name="bairro" ><br>
        Cidade: <br>
        <input type="text" name="cidade" ><br>
        CEP: <br>
        <input type="text" name="cep" size="8"><br>
        <input type="submit" name="enviar" value="Enviar" >
        <input type="reset" name="limpa" value="Limpar">
    </form>
@endsection