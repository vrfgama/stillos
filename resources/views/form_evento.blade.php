@extends('template.modelo')

@section('container')
    <h3>Formulário de cadastro de eventos</h3>
    <form action=" {{ url('/stillos/cad_evento')}} " method="post" >
        {{ csrf_field() }}
        Nome: <br>
        <input type="text" name="nome" ><br>
        Local: <br>
        <input type="text" name="local" ><br>
        Endereço: <br>
        Rua: <br>
        <input type="text" name="rua" ><br>
        Número: <br>
        <input type="text" name="numero" size="2"><br>
        Bairro: <br>
        <input type="text" name="bairro" ><br>
        Cidade: <br>
        <input type="text" name="cidade" ><br>
        <!--
        CEP: <br>
        <input type="text" name="cep" size="8"><br>
        -->
        Data: <br>
        <input type="date" name="data" size="10"><br>
        Quantidade de mesas: <br>
        <input type="text" name="qtd_mesas" size="5"><br>
        <input type="submit" name="enviar" value="Enviar" >
        <input type="reset" name="limpa" value="Limpar">
    </form>
@endsection