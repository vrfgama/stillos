@extends('template.modelo')

@section('container')
<h3>Remover reserva de mesas</h3><br>

Cliente: {{ $c->nome }} <br>
CPF: {{ $c->cpf }} <br><br>

Evento: {{ $e->nome }} <br>
Local: {{ $e->local }} <br>
Total de mesas: {{ $e->qtd_mesas }} <br><br>

Mesas ja reservadas: 
@foreach($m as $mesa)
    {{ $mesa->mesa_reservada }}
@endforeach
<br><br>

<form action='{{ url("/stillos/remover_reserva/{$e->id}/{$c->id}" ) }}' method="post">
    {{ csrf_field() }}
    Número da mesa que deseja remover: <input action="text" name="res" size="8"><br>
    <input type="submit" name="enviar" value="Remover">
    <input type="reset" name="limpa" value="Limpar">
</form>
@endsection