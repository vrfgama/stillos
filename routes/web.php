<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::group(['prefix'=>'/stillos'], function(){
    Route::get('/', 'EventoController@index');
    Route::get('/form_cliente', 'ClienteController@formulario');

    Route::get('/lista_clientes', 'ClienteController@lista');
    Route::get('/del_cliente/{id}', 'ClienteController@excluirView');
    Route::get('/excluir_cliente/{id}', 'ClienteController@excluir');
    Route::get('/atualizar_cliente/{id}', 'ClienteController@atualizarView');

    Route::get('/reserva_mesa/{id}', 'ReservaController@listaClientesView');
    Route::get('/reserva_mesa_evento/{ev_id}/{cl_id}', 'ReservaController@mesas');
    Route::get('/relatorio/{id}', 'ReservaController@relatorio');
    Route::get('/remover_mesa_evento/{ev_id}/{cl_id}', 'ReservaController@removerView');
                 
    Route::get('/form_evento', 'EventoController@formulario');
    Route::get('/deletar/{id}', 'EventoController@excluirView');
    Route::get('/excluir/{id}', 'EventoController@excluir');
    Route::get('/atualizar/{id}', 'EventoController@atualizarView');
    
    Route::post('/cad_cliente', 'ClienteController@cadastrar');
    Route::post('/cad_evento', 'EventoController@cadastrar');
    Route::post('/editar_cliente', 'ClienteController@atualizar' );
    Route::post('/editar_evento', 'EventoController@atualizar' );

    Route::post('/reserva/{ev_id}/{cl_id}', 'ReservaController@reservar');
    Route::post('/remover_reserva/{ev_id}/{cl_id}', 'ReservaController@remover');
});
